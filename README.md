# Пример SSO с IdentityServer4
## Структура проекта
 - src\
   - IdentityServer\ - шаблон сгенерированный из [Skoruba.IdentityServer4.Admin](https://github.com/skoruba/IdentityServer4.Admin#skorubaidentityserver4admin)
     - IS4SSO.STS.Identity - IdentityServer4 с фронтом для аутентификации
     - IS4SSO.Admin - админка IdentityServer4 на MVC
     - IS4SSO.Admin.Api - предоставляет доступ к админки с API
     - IS4SSO.Admin.EntityFramework.Shared - общие библиотеки для админки (сущности, context)
   - IS4SSO.WebApi1 - реализация простой api для App1 и хранениеем прав и ролей 
   - IS4SSO.WebApi2 - реализация простой api для App2 и хранениеем прав и ролей
   - IS4SSO.Data - храняться общие сущности и context
   - IS4SSO.Services - общие сервисы для Api
   - IS4SSO.App1 - небольшое приложение для взаимодействия с Api1
   - IS4SSO.App2 - небольшое приложение для взаимодействия с Api2

## Требования для запуска приложения
- Dotnet core sdk 2.2.300
- PostgreSQL
- NodeJs
- VueJS

## Подготовка IdentityServer4 к запуску 
Для запуска IdentityServer4 нужно попроавить строку подключения к PostgreSQL в проектах IS4SSO.STS.Identity, IS4SSO.Admin и IS4SSO.Admin.Api в секции ConnectionString файла appsettings.json.
Затем нужно применить миграцию в проекте IS4SSO.Admin

    dotnet ef database update -c AdminIdentityDbContext
    dotnet ef database update -c AdminLogDbContext
    dotnet ef database update -c IdentityServerConfigurationDbContext
    dotnet ef database update -c IdentityServerPersistedGrantDbContext

## Подготовка к запуску клиенстких приложении
Для подготовки клиентских приложении IS4SSO.App нужно выполнить в команду

    npm i

## Запуск проектов
IS4SSO.STS.Identity

    dotnet run

IS4SSO.Admin

    dotnet run

IS4SSO.Admin.Api

    dotnet run

IS4SSO.Admin

    dotnet run

IS4SSO.Admin.Api

    dotnet run

IS4SSO.WebApi1

    dotnet run

IS4SSO.WebApi2

    dotnet run

IS4SSO.App1

    npm run serve


IS4SSO.App2

    npm run serve

