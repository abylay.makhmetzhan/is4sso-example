using System;

namespace IS4SSO.Data.Entities
{
    public class RolePermission
    {
        public Guid RoleId { get; set; }

        public virtual Role User { get; set; }

        public Guid PermissionId { get; set; }

        public virtual Permission Permission { get; set; }
    }
}
