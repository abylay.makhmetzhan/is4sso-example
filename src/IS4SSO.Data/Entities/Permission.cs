namespace IS4SSO.Data.Entities
{
    public class Permission : Entity
    {
        public string Name { get; set; }

        public string PublicCode { get; set; }
    }
}
