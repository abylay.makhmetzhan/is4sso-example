using System;

namespace IS4SSO.Data.Entities
{
    public class UserPermission
    {
        public Guid UserId { get; set; }

        public virtual User User { get; set; }

        public Guid PermissionId { get; set; }

        public virtual Permission Permission { get; set; }
    }
}
