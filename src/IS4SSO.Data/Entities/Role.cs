using Microsoft.AspNetCore.Identity;
using System;

namespace IS4SSO.Data.Entities
{
    public class Role : IdentityRole<Guid>, IEntity<Guid>
    {
    }
}
