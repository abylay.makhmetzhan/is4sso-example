using Microsoft.AspNetCore.Identity;
using System;

namespace IS4SSO.Data.Entities
{
    public class User : IdentityUser<Guid>, IEntity<Guid>
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
