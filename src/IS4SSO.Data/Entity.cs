using System;

namespace IS4SSO.Data
{
    public class Entity : IEntity<Guid>
    {
        public Guid Id { get; set; }
    }
}
