using IS4SSO.Data.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace IS4SSO.Data
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, Guid>
    {
        public DbSet<UserPermission> UserPermissions { get; set; }

        public DbSet<RolePermission> RolePermissions { get; set; }

        public DbSet<Permission> Permissions { get; set; }

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<UserPermission>()
                .HasKey(s => new { s.UserId, s.PermissionId });

            builder.Entity<RolePermission>()
                .HasKey(s => new { s.RoleId, s.PermissionId });

            base.OnModelCreating(builder);
        }
    }
}
