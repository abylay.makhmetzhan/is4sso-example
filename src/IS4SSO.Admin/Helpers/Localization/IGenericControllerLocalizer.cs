﻿using Microsoft.Extensions.Localization;

namespace IS4SSO.Admin.Helpers.Localization
{
    public interface IGenericControllerLocalizer<T> : IStringLocalizer<T>
    {
        
    }
}