using Skoruba.IdentityServer4.Admin.BusinessLogic.Identity.Dtos.Identity;
using System.ComponentModel.DataAnnotations;

namespace IS4SSO.Admin.Models
{
    public class IdentityUserDto<TKey> : UserDto<TKey>
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
    }
}
