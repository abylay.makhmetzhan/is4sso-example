using IS4SSO.Data;
using IS4SSO.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IS4SSO.Services.UserManagers
{
    public class SecureUserManager : UserManager<User>
    {
        private readonly ApplicationDbContext _context;

        public SecureUserManager(
            ApplicationDbContext context,
            IUserStore<User> store,
            IOptions<IdentityOptions> optionsAccessor,
            IPasswordHasher<User> passwordHasher,
            IEnumerable<IUserValidator<User>> userValidators,
            IEnumerable<IPasswordValidator<User>> passwordValidators,
            ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber errors,
            IServiceProvider services,
            ILogger<UserManager<User>> logger)
            : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            _context = context;
        }

        public async Task<List<Permission>> GetPermissions(User user)
        {
            var roles = await _context.UserRoles.Where(s => s.UserId == user.Id).Select(s => s.RoleId).ToListAsync();
            var permissions = await _context.UserPermissions.Include(s => s.Permission).Where(s => s.UserId == user.Id).Select(s => s.Permission).ToListAsync();
            var rolePermissions = await _context.RolePermissions.Where(s => roles.Contains(s.RoleId)).Select(s => s.Permission).ToListAsync();
            permissions.AddRange(rolePermissions);

            return permissions.GroupBy(s => s.Id).Select(s => s.First()).ToList();
        }
    }
}
