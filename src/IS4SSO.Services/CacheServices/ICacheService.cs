using System;
using System.Threading.Tasks;

namespace IS4SSO.Services.CacheServices
{
    public interface ICacheService
    {
        Task<T> GetData<T>(string key) where T : class;

        Task SetData<T>(string key, T data, TimeSpan? expired = null) where T : class;
    }
}
