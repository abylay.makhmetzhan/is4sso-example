using Microsoft.Extensions.Caching.Memory;
using System;
using System.Threading.Tasks;

namespace IS4SSO.Services.CacheServices
{
    public class MemoryCache : ICacheService
    {
        private readonly IMemoryCache _memoryCache;

        public MemoryCache(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        public Task<T> GetData<T>(string key) where T : class
        {
            if (_memoryCache.TryGetValue(key, out T data))
            {
                return Task.FromResult(data);
            }

            return Task.FromResult<T>(null);
        }

        public Task SetData<T>(string key, T data, TimeSpan? expired = null) where T : class
        {
            if (expired == null)
                _memoryCache.Set(key, data);
            else
                _memoryCache.Set(key, data, expired.Value);

            return Task.CompletedTask;
        }
    }
}
