using IS4SSO.WebApi2.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IS4SSO.WebApi2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Policy = "Values")]
    public class ValuesController : ControllerBase
    {
        private readonly IAuthorizationService _auth;

        public ValuesController(IAuthorizationService authorizationService)
        {
            _auth = authorizationService;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        [Authorize(Policy = "ChangeValues")]
        public void Post([FromBody] Value value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        [Authorize(Policy = "ChangeValues")]
        public void Put(int id, [FromBody] Value value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var access = await _auth.AuthorizeAsync(User, "ChangeValues");
            if (access.Succeeded)
            {
                return Ok();
            }

            return Forbid();
        }
    }
}
