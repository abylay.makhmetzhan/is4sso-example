using IS4SSO.Data.Entities;
using IS4SSO.Services.CacheServices;
using IS4SSO.WebApi2.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IS4SSO.WebApi2.Common
{
    public class CustomJwtBearerEvents : JwtBearerEvents
    {
        private readonly ICacheService _cacheService;

        public CustomJwtBearerEvents(ICacheService cache)
        {
            _cacheService = cache;
        }

        public override async Task TokenValidated(TokenValidatedContext context)
        {
            if (context.Principal.Identity is ClaimsIdentity identity)
            {
                var tokenId = context.Principal.GetJwtId();
                var permissions = await _cacheService.GetData<List<Permission>>(tokenId) ?? new List<Permission>();

                foreach (var item in permissions)
                {
                    identity.AddClaim(new Claim(item.PublicCode, string.Empty));
                }
            }
            await base.TokenValidated(context);
        }
    }
}
