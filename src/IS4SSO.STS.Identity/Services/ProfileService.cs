using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IS4SSO.Admin.EntityFramework.Shared.Entities.Identity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IS4SSO.STS.Identity.Services
{
    public class ProfileService : IProfileService
    {
        private readonly UserManager<UserIdentity> _userManager;
        private readonly IUserClaimsPrincipalFactory<UserIdentity> _claimsFactory;

        public ProfileService(UserManager<UserIdentity> userManager, IUserClaimsPrincipalFactory<UserIdentity> claimsFactory)
        {
            _userManager = userManager;
            _claimsFactory = claimsFactory;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            string sub = context.Subject?.GetSubjectId();
            if (sub == null)
            {
                throw new Exception("No sub claim present");
            }

            var user = await _userManager.GetUserAsync(context.Subject);

            ClaimsPrincipal claimsPrincipal = await _claimsFactory.CreateAsync(user);
            if (claimsPrincipal == null)
            {
                throw new Exception("ClaimsFactory failed to create a principal");
            }
            context.AddRequestedClaims(claimsPrincipal.Claims);

            var profile = new IdentityResources.Profile();
            if (context.RequestedResources.IdentityResources.Any(s => s.Name == profile.Name))
            {
                var claims = new List<Claim>
                {
                    new Claim("FirstName", user.FirstName),
                    new Claim("LastName", user.LastName),
                };
                context.IssuedClaims.AddRange(claims);
            }
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var user = await _userManager.GetUserAsync(context.Subject);
            context.IsActive = (user != null);
        }
    }
}
