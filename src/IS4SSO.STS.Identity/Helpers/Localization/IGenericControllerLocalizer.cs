﻿using Microsoft.Extensions.Localization;

namespace IS4SSO.STS.Identity.Helpers.Localization
{
    public interface IGenericControllerLocalizer<T> : IStringLocalizer<T>
    {
        
    }
}