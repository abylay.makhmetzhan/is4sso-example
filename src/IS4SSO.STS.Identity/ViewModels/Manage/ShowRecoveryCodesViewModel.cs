﻿namespace IS4SSO.STS.Identity.ViewModels.Manage
{
    public class ShowRecoveryCodesViewModel
    {
        public string[] RecoveryCodes { get; set; }
    }
}
