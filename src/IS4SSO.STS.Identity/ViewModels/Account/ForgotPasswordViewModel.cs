﻿using System.ComponentModel.DataAnnotations;

namespace IS4SSO.STS.Identity.ViewModels.Account
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
