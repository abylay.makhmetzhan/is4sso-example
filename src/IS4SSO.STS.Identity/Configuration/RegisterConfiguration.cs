﻿using IS4SSO.STS.Identity.Configuration.Intefaces;

namespace IS4SSO.STS.Identity.Configuration
{
    public class RegisterConfiguration : IRegisterConfiguration
    {
        public bool Enabled { get; set; } = true;
    }
}
