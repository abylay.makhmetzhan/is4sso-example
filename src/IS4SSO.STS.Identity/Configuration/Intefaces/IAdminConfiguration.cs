﻿namespace IS4SSO.STS.Identity.Configuration.Intefaces
{
    public interface IAdminConfiguration
    {
        string IdentityAdminBaseUrl { get; }
    }
}