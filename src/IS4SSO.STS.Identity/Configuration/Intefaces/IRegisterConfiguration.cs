﻿namespace IS4SSO.STS.Identity.Configuration.Intefaces
{
    public interface IRegisterConfiguration
    {
        bool Enabled { get; }
    }
}