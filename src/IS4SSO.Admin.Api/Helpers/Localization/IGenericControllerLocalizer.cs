﻿using Microsoft.Extensions.Localization;

namespace IS4SSO.Admin.Api.Helpers.Localization
{
    public interface IGenericControllerLocalizer<T> : IStringLocalizer<T>
    {

    }
}