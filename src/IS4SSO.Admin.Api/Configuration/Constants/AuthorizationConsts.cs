﻿namespace IS4SSO.Admin.Api.Configuration.Constants
{
    public class AuthorizationConsts
    {
        public const string IdentityServerBaseUrl = "http://localhost:5000";
        public const string OidcSwaggerUIClientId = "AdminClient_api_swaggerui";
        public const string OidcApiName = "AdminClient_api";

        public const string AdministrationPolicy = "RequireAdministratorRole";
        public const string AdministrationRole = "Admin";
    }
}