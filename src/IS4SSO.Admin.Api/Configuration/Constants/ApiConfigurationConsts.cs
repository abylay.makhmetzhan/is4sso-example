﻿namespace IS4SSO.Admin.Api.Configuration.Constants
{
    public class ApiConfigurationConsts
    {
        public const string ApiName = "IS4SSO Api";

        public const string ApiVersionV1 = "v1";
    }
}
