using IS4SSO.Data.Entities;
using IS4SSO.Services.CacheServices;
using IS4SSO.Services.UserManagers;
using IS4SSO.WebApi1.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace IS4SSO.WebApi1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PermissionsController : ControllerBase
    {
        private readonly SecureUserManager _userManager;
        private readonly ICacheService _cache;

        public PermissionsController(SecureUserManager userManager, ICacheService cache)
        {
            _userManager = userManager;
            _cache = cache;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]User userInfo)
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                user = userInfo;
                user.UserName = user.Email;
                user.EmailConfirmed = true;
                await _userManager.CreateAsync(user);
            }
            else if (user.Id != userInfo.Id)
            {
                return BadRequest();
            }
            else if (user.LastName != userInfo.LastName || user.FirstName != userInfo.FirstName)
            {
                user.FirstName = userInfo.FirstName;
                user.LastName = userInfo.LastName;
                await _userManager.UpdateAsync(user);
            }
            var permissions = await _userManager.GetPermissions(user);

            var jwtId = User.GetJwtId();
            var exp = User.GetExpirationTime() - DateTime.Now;

            await _cache.SetData(jwtId, permissions, exp);

            return Ok(permissions);
        }
    }
}
