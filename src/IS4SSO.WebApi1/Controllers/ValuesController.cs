using IS4SSO.WebApi1.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace IS4SSO.WebApi1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Policy = "Values")]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        [Authorize(Policy = "ChangeValues")]
        public void Post([FromBody] Value value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        [Authorize(Policy = "ChangeValues")]
        public void Put(int id, [FromBody] Value value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        [Authorize(Policy = "ChangeValues")]
        public void Delete(int id)
        {
        }
    }
}
