using System;
using System.Linq;
using System.Security.Claims;

namespace IS4SSO.WebApi1.Extensions
{
    public static class ClaimsPrincipalExtension
    {
        public static string GetJwtId(this ClaimsPrincipal principal)
        {
            return principal.Claims.Single(s => s.Type == "jti").Value;
        }

        public static DateTime GetExpirationTime(this ClaimsPrincipal principal)
        {
            var exp = int.Parse(principal.Claims.Single(s => s.Type == "exp").Value);
            return DateTimeOffset.FromUnixTimeSeconds(exp).ToLocalTime().DateTime;
        }
    }
}
