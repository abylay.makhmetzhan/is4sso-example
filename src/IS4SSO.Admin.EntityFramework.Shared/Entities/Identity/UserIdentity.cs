﻿using Microsoft.AspNetCore.Identity;

namespace IS4SSO.Admin.EntityFramework.Shared.Entities.Identity
{
    public class UserIdentity : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
