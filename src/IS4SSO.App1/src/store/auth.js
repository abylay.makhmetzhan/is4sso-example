import security from '../services/security'
import HTTP from '../services/http-common'

export default {
  state: {
    user: null,
    permissions: []
  },
  getters: {
    isAuthorized: state => !(state.user == null),
    getToken: state => {
      if(state.user && state.user.access_token){
        return state.user.access_token
      }
      return null
    },
    getPermissions: state => state.permissions,
    getUser: state => state.user
  },
  mutations: {
    setUser(state, payload){
      state.user = payload
    },
    setPermissions(state, payload){
      state.permissions = payload
    }
  },
  actions: {
    login(){
      security.signIn()
    },
    logout({commit}) {
      commit('setUser', null)
      return security.signoutRedirect()
    },
    getUser({commit}){
      return new Promise((resolve, reject) => {
        security.getUser().then(user => {
          if (user) {
            commit('setUser', user)
            resolve()
          } else {
            reject()
          }
        }).catch(() => {
          reject()
        })
      })
    },
    renewToken(){
      return security.renewToken()
    },
    getPermissions({commit, state}){
      let user = {
        Id: state.user.profile.sub,
        Email: state.user.profile.email,
        FirstName: state.user.profile.FirstName,
        LastName: state.user.profile.LastName
      }

      return new Promise((resolve, reject) => {
        HTTP.get("/api/Permissions", {
          params: user
        })
          .then(response => {
            commit('setPermissions', response.data)
            resolve()
          })
          .catch(() => {
            reject()
          })
      })
    }
  }
}
