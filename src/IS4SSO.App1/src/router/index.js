import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/views/Home.vue'),
      meta: { authorize: false }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/Login.vue'),
      meta: { authorize: false }
    },
    {
      path: '/unauthorized',
      name: 'Unauthorized',
      component: () => import('@/views/Unauthorized.vue'),
      meta: { authorize: false }
    },
    {
      path: '/callback',
      name: 'callback',
      component: () => import('@/views/Callback.vue'),
      meta: { authorize: false }
    },
    {
      path: '/silent-renew',
      name: 'silentRenew',
      component: () => import('@/views/SilentRenew.vue'),
      meta: { authorize: false }
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('@/views/About.vue'),
      meta: { authorize: true }
    }
  ]
})

router.beforeEach((to, from, next) => {
  const isAuth = store.getters.isAuthorized
  const requaireAuth = to.matched.some(record => record.meta.authorize)

  if(requaireAuth && !isAuth){
    next({ name: 'login'})
  } else {
    next()
  }
})

export default router
