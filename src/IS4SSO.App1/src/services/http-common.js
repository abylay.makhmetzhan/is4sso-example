import axios from 'axios'
import store from '../store'

const axiosApi = axios.create({
   baseURL: process.env.VUE_APP_BASE_URL
})

axiosApi.interceptors.request.use(
   (config) => {
     let isUserAuthenticated = store.getters.isAuthorized
     let token = store.getters.getToken

     if (isUserAuthenticated && token) {
       config.headers['Authorization'] = `Bearer ${ token }`
     }
 
     return config
   },
 
   (error) => {
     return Promise.reject(error)
   }
 )

export default axiosApi;
