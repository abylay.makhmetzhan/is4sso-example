import HTTP from '../services/http-common'

export default {
  state: {
    loading: false,
  },
  getters: {
    getLoading: state => state.loading
  },
  mutations: {
    setLoading(state, value){
      state.loading = value
    },
  },
  actions: {
    getValues({commit}){
      commit('setLoading', true)
      return new Promise((resolve, reject) => {
        HTTP.get("/api/values")
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error)
          })
          .finally(() => {
            commit('setLoading', false)
          })
      })
    },
    getValue({commit}, id){
      commit('setLoading', true)
      return new Promise((resolve, reject) => {
        HTTP.get("/api/values/" + id)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error)
          })
          .finally(() => {
            commit('setLoading', false)
          })
      })
    },
    postValue({commit}, value){
      commit('setLoading', true)
      return new Promise((resolve, reject) => {
        HTTP.post("/api/values/", value)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error)
          })
          .finally(() => {
            commit('setLoading', false)
          })
      })
    },
    putValue({commit}, data){
      commit('setLoading', true)
      return new Promise((resolve, reject) => {
        HTTP.put("/api/values/" + data.id, data.payload)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error)
          })
          .finally(() => {
            commit('setLoading', false)
          })
      })
    },
    deleteValue({commit}, value){
      commit('setLoading', true)
      return new Promise((resolve, reject) => {
        HTTP.delete("/api/values/" + value)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error)
          })
          .finally(() => {
            commit('setLoading', false)
          })
      })
    },
  }
}
