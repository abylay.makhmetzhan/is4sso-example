import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import authModule from './auth'
import apiModule from './api'

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    authModule,
    apiModule
  },
  plugins: [createPersistedState()]
})

export default store
